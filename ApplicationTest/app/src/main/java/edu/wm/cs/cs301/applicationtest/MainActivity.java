package edu.wm.cs.cs301.applicationtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

public class MainActivity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;
    private ImageView audioon;
    private ImageView audioof;
    private TextView text;
    private Boolean mute = false;
    private int score;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3237570911041454~3609150323");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());



        text = findViewById(R.id.highscoretext);

        SharedPreferences preferences = getSharedPreferences("game", MODE_PRIVATE);

        score = preferences.getInt("highscore",0);
        text.setText("" + score);


        audioon = findViewById(R.id.volume);
        audioof = findViewById(R.id.volumeoff);

        audioof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                audioof.setVisibility(View.INVISIBLE);
                audioon.setVisibility(View.VISIBLE);
                mute = false;
            }
        });
        audioon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                audioon.setVisibility(View.INVISIBLE);
                audioof.setVisibility(View.VISIBLE);
                mute = true;
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            }
        });


        findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,GameActivity.class);
                intent.putExtra("mute",mute);
                startActivity(intent);
            }
        });
    }

}