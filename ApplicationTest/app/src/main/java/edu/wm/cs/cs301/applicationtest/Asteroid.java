package edu.wm.cs.cs301.applicationtest;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

public class Asteroid {

    public int speed = 25;
    int x,y, width, height;
    int assCounter = 1;
    Bitmap asteroid1, asteroid2;
    Bitmap rip;
    Random random;

    private int multiplier = 2;

    Asteroid (Resources res){
        asteroid1 = BitmapFactory.decodeResource(res,R.drawable.cookie1);
        asteroid2 = BitmapFactory.decodeResource(res,R.drawable.cookie2);
        rip = BitmapFactory.decodeResource(res,R.drawable.boom);

        width = asteroid1.getWidth();
        height = asteroid1.getHeight();

        random = new Random();
        int low = 2;
        int high = 6;
        multiplier = random.nextInt(high-low) + low;

        width*=multiplier;
        height*=multiplier;

        asteroid1 = Bitmap.createScaledBitmap(asteroid1, width, height,false);
        asteroid2 = Bitmap.createScaledBitmap(asteroid2, width, height,false);
        rip = Bitmap.createScaledBitmap(rip, width, height,false);
        y = -height;

    }

    int getSpeed(){
        return 100/multiplier + random.nextInt(30);
    }

    Bitmap getAsteroid() {
        if (assCounter < 3) {
            assCounter++;
            return asteroid1;
        }

        else if (assCounter >=3 & assCounter < 4) {
            assCounter --;
            return asteroid2;
        } else {
            assCounter = 1;
            return asteroid2;
        }
    }

    Rect getCollision(){
        return new Rect(x+100,y+100,x+width-(width/4),y+height-height/2);
    }

}
