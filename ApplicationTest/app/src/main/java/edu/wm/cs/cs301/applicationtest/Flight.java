package edu.wm.cs.cs301.applicationtest;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import static edu.wm.cs.cs301.applicationtest.GameView.screenRatioX;
import static edu.wm.cs.cs301.applicationtest.GameView.screenRatioY;

public class Flight {
    public boolean isGoingUp = false;
    public int toShoot = 0;
    int x,y, width, height, counter = 0;
    Bitmap flight1;
    Bitmap flight2;
    Bitmap rip;
    private GameView gameView;

    Flight(GameView gameView, int screenY, Resources res){

        this.gameView = gameView;

        flight1 = BitmapFactory.decodeResource(res,R.drawable.ship1);
        flight2 = BitmapFactory.decodeResource(res,R.drawable.ship2);
        rip = BitmapFactory.decodeResource(res,R.drawable.boom);

        width = flight1.getWidth();
        height = flight1.getHeight();

        width*=2;
        height*=2;

        flight1 = Bitmap.createScaledBitmap(flight1, width, height,false);
        flight2 = Bitmap.createScaledBitmap(flight2, width, height,false);
        rip = Bitmap.createScaledBitmap(rip, width, height,false);

        y = screenY/2;
        x = 100;
    }

    Bitmap getFlight (){
        if (toShoot!=0) {
            toShoot --;
            gameView.newBullet();
        }

        if (counter == 0){
            counter += 1;
            return flight1;
        }
        else{
            counter -= 1;
            return flight2;
        }
    }
    Rect getCollision(){
        return new Rect(x,y,x+width,y+height);
    }

    Bitmap getRip(){
        return rip;
    }

}
