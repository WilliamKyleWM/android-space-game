package edu.wm.cs.cs301.applicationtest;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Background {
    int x = 0,y = 0;
    Bitmap background;

    Background(int x, int y, Resources res){
        background = BitmapFactory.decodeResource(res, R.drawable.space);
        background = Bitmap.createScaledBitmap(background, x, y, false);

    }
}
