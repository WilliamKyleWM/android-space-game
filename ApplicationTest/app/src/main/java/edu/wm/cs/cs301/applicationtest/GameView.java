package edu.wm.cs.cs301.applicationtest;

import java.io.IOException;
import java.util.ArrayList;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.core.view.GestureDetectorCompat;

public class GameView extends SurfaceView implements Runnable {

    private Thread thread;
    private Boolean playing = true;

    private Button shoot;

    private int score = 0;
    private SharedPreferences preferences;
    private float offsetX, offsetY;
    private boolean touching = false;

    private SoundPool pool;
    private int expl, pew, die, fly, level,song;

    private Background bg1;
    private Background bg2;
    private int screenX;
    private int screenY;
    private Paint paint;
    private Flight flight;
    private Random random;

    private List<Bullet> bullets;
    private Asteroid[] asteroids;
    private List<xy> explosions;
    private List<Integer> xes;

    private TextView resume;
    private TextView pause;

    public static float screenRatioX;
    public static float screenRatioY;
    Canvas canvas;
    private boolean gameOver = false;
    private boolean both;
    private boolean explode;
    MediaPlayer mediaPlayer;


    private GameActivity context;
    private AtomicInteger difficulty;

    private static final String DEBUG_TAG = "Gestures";
    private GestureDetectorCompat mDetector;
    private int mActivePointerId;
    private int mSecondaryId;
    private int bullet_counter;


    public GameView(GameActivity context, int x, int y) {
        super(context);
        //paint = new Paint();
        this.context = context;
        preferences = context.getSharedPreferences("game", Context.MODE_PRIVATE);
        bg1 = new Background(x,y,getResources());
        bg2 = new Background(x,y,getResources());

        this.screenX = x;
        this.screenY = y;




        // Set the gesture detector as the double tap
        // listener.


        flight = new Flight(this, y, getResources());
        bullets = new ArrayList<>();



        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
            pool = new SoundPool.Builder().setMaxStreams(12).setAudioAttributes(audioAttributes).build();
        } else {
            pool = new SoundPool(12, AudioManager.STREAM_MUSIC, 0 );
        }

        expl = pool.load(getContext(),R.raw.explode,1);
        pew = pool.load(getContext(),R.raw.pew,1);
        die = pool.load(getContext(),R.raw.die,1);
        fly = pool.load(getContext(),R.raw.fly,1);
        level = pool.load(getContext(),R.raw.level,1);

        if (!context.mute){
            mediaPlayer = MediaPlayer.create(context, R.raw.song);
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
            }

            }


        screenRatioX = 1920f/x;
        screenRatioY = 1080f/y;
        bg2.x = x;

        difficulty = new AtomicInteger(6);

        random = new Random();
    }

    @Override
    public void run() {
        while (playing){
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
            }

            if (bullet_counter == 5){
                if (!context.mute){
                pool.play(level,1,1,0,1,1);}
                asteroids = new Asteroid[6];

                for (int i = 0; i < 6; i++) {
                    Asteroid asteroid = new Asteroid(getResources());
                    asteroids[i] = asteroid;
                }
            }

            if (bullet_counter%100 == 0){
                if (bullet_counter!=0){
                    if (!context.mute){
                pool.play(level,1,1,0,1,1);}}
                asteroids = new Asteroid[bullet_counter/10];

                    for (int i = 0; i < bullet_counter/10; i++) {
                        Asteroid asteroid = new Asteroid(getResources());
                        asteroids[i] = asteroid;
                    }
                }


            update();
            draw();
            sleep();
        }
        if (!context.mute) {
            pool.play(die, 1, 1, 0, 0, 1);
            mediaPlayer.stop();
        }
    }

    private void draw() {
        if (getHolder().getSurface().isValid()){
            Canvas canvas = getHolder().lockCanvas();
            canvas.drawBitmap(bg1.background, bg1.x, bg1.y, paint);
            canvas.drawBitmap(bg2.background, bg2.x, bg2.y, paint);

            if (gameOver){
                playing = false;
                canvas.drawBitmap(flight.getRip(),flight.x,flight.y,paint);
                highScore();
                waitExit();

                getHolder().unlockCanvasAndPost(canvas);
                return;
            }
            if (explode) {
                for (xy i : explosions) {
                    if (!context.mute) {
                        random.nextInt();
                        float rate = random.nextInt(2)+2/2;
                        pool.play(expl, 1, 1, 0, 0, rate);
                    }
                    canvas.drawBitmap(flight.getRip(), i.getX(), i.getY(), paint);
                }
            }

            for (Asteroid asteroid:asteroids){
                canvas.drawBitmap(asteroid.getAsteroid(), asteroid.x, asteroid.y,paint);
            }

            canvas.drawBitmap(flight.getFlight(),flight.x,flight.y,paint);

            for (Bullet bullet : bullets){
                canvas.drawBitmap(bullet.bullet,bullet.x, bullet.y,paint);
            }
            paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setTextSize(100);
            canvas.drawText("Score= " + score,100,100,paint);

            if (bullet_counter<5) {
                canvas.drawText("Touch the left", screenX / 2 - screenX / 4 - 400, screenY / 2, paint);
                canvas.drawText("side to fly!", screenX / 2 - screenX / 4 - 400, screenY / 2+85, paint);
                canvas.drawText("Touch the right", screenX / 2 + screenX / 4 - 300, screenY / 2, paint);
                canvas.drawText("side to shoot!", screenX / 2 + screenX / 4 - 250, screenY / 2 + 85, paint);
            }

            if (bullet_counter > 4 && bullet_counter<8){
                paint.setTextSize(100);
                canvas.drawText("Shoot the asteroids!", screenX/2,screenY/2+300,paint);
            }

            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void waitExit() {
        try {
            Thread.sleep(100);
            Intent intent = new Intent(context,MainActivity.class);
            context.startActivity(intent);
            context.finish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void highScore() {
        if (preferences.getInt("highscore",0)<score){
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("highscore",score);
            editor.apply();
        }


    }

    private void update(){

        bg1.x -= 10;
        bg2.x -= 10;

        if (bg1.x+bg1.background.getWidth()<0){
            bg1.x = screenX;
        }
        if (bg2.x+bg2.background.getWidth()<0){
            bg2.x = screenX;
        }

        if (flight.isGoingUp){
            flight.y-=30;
        }
        else{
            flight.y +=30;
        }

        if (flight.y<0){
            flight.y = 0;
        }
        if (flight.y>screenY - flight.height){
            flight.y = screenY - flight.height;
        }

        List<Bullet> trash = new ArrayList<>();
        explode = false;
        explosions = new ArrayList<xy>();

        for (Bullet bullet : bullets){
            if (bullet.x>screenX){
                trash.add(bullet);}
             bullet.x+=50;


            for (Asteroid asteroid: asteroids){
                if (Rect.intersects(asteroid.getCollision(),bullet.getCollision())){
                    xy hold = new xy(asteroid.x,asteroid.y);
                    explosions.add(hold);
                    score += 10;
                    explode = true;
                    asteroid.x=-500;
                    bullet.x = screenX+100;
                }
            }

        }
        for (Bullet bullet: trash){
            bullets.remove(bullet);
        }

        for (Asteroid asteroid: asteroids){
            asteroid.x -= asteroid.getSpeed();

            if (asteroid.x + asteroid.width < 0){
                asteroid.speed = random.nextInt(35);

                if (asteroid.getSpeed()<10){
                    asteroid.speed = 10;
                }
                asteroid.x = screenX;
                asteroid.y = random.nextInt(screenY-asteroid.height);
            }
            if (Rect.intersects(asteroid.getCollision(),flight.getCollision())){
                gameOver = true;
                return;
            }
        }
    }
    private void sleep(){
        try {
            Thread.sleep(1000/60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void resume(){
        playing = true;
        thread = new Thread(this);
        thread.start();
    }

    public void pause(){
        try {
            playing = false;
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                if (event.getX(event.getActionIndex()) < screenX / 2) {
                    flight.isGoingUp = true;
                }
                if (event.getX(event.getActionIndex()) > screenX / 2){
                    flight.toShoot++;}
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                if (event.getX(event.getActionIndex()) < screenX / 2) {
                    flight.isGoingUp = true;
                }

                if (event.getX(event.getActionIndex()) > screenX / 2)
                    flight.toShoot++;

                break;
            case MotionEvent.ACTION_UP:
                if (event.getX(event.getActionIndex()) < screenX / 2){
                    flight.isGoingUp=false;
                }
                break;

            case MotionEvent.ACTION_POINTER_UP:
                if (event.getX(event.getActionIndex()) < screenX / 2){
                    flight.isGoingUp=false;
            }
            break;
        }

        return true;
    }


    public void newBullet() {

        Bullet bullet = new Bullet(getResources());
        bullet.x = flight.x+flight.width-50;
        bullet.y = flight.y+1;
        if (!context.mute) {
            pool.play(pew, 1, 1, 0, 0, 1);
        }
        bullet_counter +=1;
        bullets.add(bullet);
    }

    }




